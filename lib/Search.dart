import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:food_menu_app_ui/constrant.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:developer';

class SearchScreen extends StatefulWidget {
  SearchScreen({Key? key}) : super(key: key);

  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  @override
  final Future<FirebaseApp> firebase = Firebase.initializeApp();
  final Stream<QuerySnapshot> _foods =
      FirebaseFirestore.instance.collection('foods').snapshots();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: StreamBuilder<QuerySnapshot>(
        stream: _foods,
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else
            return Padding(
              padding: const EdgeInsets.all(30.0),
              child: ListView(
                children: [
                  Row(
                    children: [
                      Container(
                        child: Text(
                          "Search List",
                          style: TextStyle(
                            fontSize: 25.00,
                            color: Theme.of(context).primaryColor,
                            fontWeight: FontWeight.w800,
                          ),
                        ),
                      )
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 16.00),
                    child: TextField(
                      style: TextStyle(
                        color: Theme.of(context).primaryColor,
                      ),
                      decoration: InputDecoration(
                        prefixIcon: Icon(
                          Icons.search,
                          color: Theme.of(context).primaryColor,
                        ),
                        hintStyle: TextStyle(
                          color: ColorWhite,
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide: BorderSide(
                            color: Theme.of(context).primaryColor,
                            width: 1.0,
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30),
                          borderSide: BorderSide(
                            color: ColorYellow,
                            width: 2.0,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      top: 20,
                    ),
                    child: Column(
                      children: snapshot.data!.docs.map((doc) {
                        return Row(
                          children: [
                            Text(
                              doc['name'],
                              style: TextStyle(
                                fontSize: 20.00,
                                color: Theme.of(context).primaryColor,
                                fontWeight: FontWeight.w200,
                              ),
                            )
                          ],
                        );
                      }).toList(),
                    ),
                  )
                ],
              ),
            );
        },
      ),
      // body: ListView(
      //   children: [
      //     Text('data'),
      //     StreamBuilder<QuerySnapshot>(
      //       stream: _foods,
      //       builder: (context, snapshot) {
      //         // if (!snapshot.hasData) {
      //         //   return Center(
      //         //     child: CircularProgressIndicator(),
      //         //   );
      //         // } else
      //         //   print("ควยยุม");
      //         // snapshot.data!.docs.map((e) {
      //         //   return Text(e.toString());
      //         // }).toList();
      //         print(snapshot.data!.docs.toString());
      //         return Center(
      //           child: CircularProgressIndicator(),
      //         );
      //       },
      //     ),
      //   ],
      // ),
      // body: ListView(
      //   children: <Widget>[
      //
      //   ],
      // child:
      // child: Column(
      //   // crossAxisAlignment: CrossAxisAlignment.start,
      //   children: [

      //   ],
      // ),
    );
  }
}
