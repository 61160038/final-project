import 'package:flutter/material.dart';
import 'package:food_menu_app_ui/Home.dart';
import 'package:food_menu_app_ui/List.dart';
import 'package:food_menu_app_ui/Search.dart';
import 'package:food_menu_app_ui/Setting.dart';
import 'package:food_menu_app_ui/constrant.dart';

class Navbottombar extends StatefulWidget {
  int? index;
  Navbottombar({
    Key? key,
    @required this.index,
  }) : super(key: key);
  @override
  _NavbottombarState createState() => _NavbottombarState();
}

class _NavbottombarState extends State<Navbottombar> {
  int _index = 0;

  @override
  void initState() {
    if (widget.index != null) {
      setState(() {
        _index = widget.index!;
      });
    } else {
      setState(() {
        _index = 0;
      });
    }
    super.initState();
  }

  final List<Widget> _pages = <Widget>[
    HomeScreen(),
    SearchScreen(),
    SettingScreen(),
  ];

  final List<BottomNavigationBarItem> _menuItems = <BottomNavigationBarItem>[
    BottomNavigationBarItem(
      icon: Column(
        children: [
          Container(
            margin: EdgeInsets.only(bottom: 5),
            child: ImageIcon(
              AssetImage("./assets/icons/homeicon.png"),
            ),
          ),
        ],
      ),
      label: "Home",
    ),
    BottomNavigationBarItem(
      icon: Container(
        margin: EdgeInsets.only(bottom: 5),
        child: ImageIcon(
          AssetImage("./assets/icons/search.png"),
        ),
      ),
      label: "SearchList",
    ),
    BottomNavigationBarItem(
      icon: Container(
        margin: EdgeInsets.only(bottom: 5),
        child: ImageIcon(
          AssetImage("./assets/icons/gear.png"),
        ),
      ),
      label: "Setting",
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: [
          Align(
              alignment: Alignment.center,
              child: Container(
                  padding: EdgeInsets.only(
                    bottom: 20,
                  ),
                  margin: EdgeInsets.only(
                    bottom: 30,
                  ),
                  child: _pages[_index])),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: const Color(0xff97ABC2).withOpacity(0.8),
                  ),
                ],
              ),
              child: Container(
                height: 80,
                child: BottomNavigationBar(
                  unselectedLabelStyle: TextStyle(
                    fontSize: 12.68,
                  ),
                  selectedLabelStyle: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 12.68,
                  ),
                  iconSize: 25,
                  backgroundColor: Theme.of(context).scaffoldBackgroundColor,
                  items: _menuItems,
                  selectedItemColor: Theme.of(context).iconTheme.color,
                  unselectedItemColor: ColorGrey,
                  currentIndex: _index,
                  onTap: (index) => setState(() => _index = index),
                  showSelectedLabels: false,
                  showUnselectedLabels: false,
                  type: BottomNavigationBarType.fixed,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
